import torch
import data_helper as data_helper
import numpy as np


class Audioclassier(torch.utils.data.Dataset):
    '''
    create audio loader, load the files from the top frequent country.
    convert every audio into a mfcc matrix. split every mfcc matrix into mun_idx 
    parts cross the time axis, in order to augment the data.
    '''

    def __init__(self, args, split):
        self._split = split
        self._path = args.path
        self._sample_rate = args.sample_rate
        self.topt = args.topt
        self._num_idx = args.num_index
        self._load_data()

    def _load_data(self):

        file_group = data_helper.dir_state(self._path)
        files, labels = data_helper.data_split(
            file_group, self.topt, split=self._split
        )

        self.raw_sample, data_idx, self.data_len = data_helper.raw_audio(
            files, self._path, self._sample_rate, self._num_idx, self._split)

        self.frame_idx = data_helper.get_keyframe(files, labels, data_idx)

    def __len__(self):

        return len(self.frame_idx)

    def __getitem__(self, idx):
        '''
        given an index return model input and lable
        '''
        audio_index, seq_index, file_name, label = self.frame_idx[idx]

        audio_get = self.raw_sample[audio_index]
        seq_len = self.data_len[audio_index]
        #seq = np.mean(audio_get[:, seq_index:seq_index+seq_len], axis=1)
        seq = audio_get[:, seq_index:seq_index + seq_len].reshape(1,13,1292)

        return torch.tensor(seq), label


def get_dataset(args, split, batch_size=128, num_workers=1):
    '''
    assemble dataloader
    '''
    dataset = Audioclassier(args, split)
    loaders = torch.utils.data.DataLoader(
        dataset,
        batch_size=batch_size,
        num_workers=num_workers,
        shuffle=True,
    )

    return loaders
