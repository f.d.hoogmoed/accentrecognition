import librosa
import numpy as np
import pyrubberband as pyrb
import matplotlib.pyplot as plt
from tqdm import tqdm
from collections import Counter
import string
import pandas as pd
import os
import time


def dir_state(path):
    '''
    group the audio files by the country name and rank the number of the files
    Args:
        path (str): path of the audio fold
    returns :
        group (dataframe):
    ltype       file_name                           file_len
    english     [english1.mp3, english2.mp3,...]    579
    spanish     [spanish1.mp3, spanish2.mp3,...]    162
    '''
    #path = r"D:\pattern_recognition\archive\recordings\recordings"
    digit = str.maketrans('', '', string.digits)
    dir_list = os.listdir(path)
    name_list = map(lambda item: item.split(".")[0].translate(digit), dir_list)
    dir_table = pd.DataFrame({"file_name": dir_list,
                              "ltype": list(name_list)})

    group = dir_table.groupby("ltype", as_index=False).agg(
        lambda items: list(items))
    group['file_len'] = group["file_name"].map(lambda item: len(item))
    group = group.sort_values(by="file_len", ascending=False)
    group.index = np.arange(len(group))

    return group


def data_split(table, topt, split="train"):
    '''
    choose the top frequent country.connecting the files in every class. 
    split data into training and validation part.
    Args:
        table (dataframe): get from the dir_state
        topt (int): top frequent country
        split (str): "train" or "val"
    Return:
        samples (narray): all the selected file names ["english1.mp3", "spanish.mp3",...]
        labels (narray): 
    '''
    subtable = table.iloc[:topt]
    print(subtable)
    t_len = subtable.iloc[-1, 2]
    split_len = round(t_len*0.8)

    if split == "train":

        samples = subtable["file_name"].map(lambda item: item[:int(split_len)])#len(item)-10
        samples = np.reshape(list(samples), (-1,))
        #realsamples = []
        #for sample in samples:
        #    realsamples = np.append(realsamples, sample)
        #samples = realsamples
#
        i = 0
        x = []
        #for fileCount in subtable["file_len"]:
        #    x = np.append(x, [i] * int(fileCount - 10))
        #    i+=1
        labels = np.arange(topt).repeat(split_len)
    elif split == "val":

        samples = subtable["file_name"].map(
            lambda item: item[int(len(item)-10):int(len(item))])
        samples = np.reshape(list(samples), (-1,))
        #realsamples = []
        #for sample in samples:
        #    realsamples = np.append(realsamples, sample)
        #samples = realsamples
        i = 0
        x = []
        #for fileCount in subtable["file_len"]:
        #    x = np.append(x, [i] * (fileCount - int(fileCount - 10)))
        #    i+=1
        labels = np.arange(topt).repeat(10)
    else:
        raise NotImplementedError("split {} is not supported".format(split))
    return samples, labels


def audio_read(path, rate):
    '''
    read audio and convert it into mfcc matrix
    Args:
        path (str): absolute path for the audio file
        rate (int): sample rate
    Return:
        specgram (narray): mfcc matrix in the shape (40, time sequence len)
    '''
    # COMMANDS = ("/ffmpeg.exe', 'avconv')
    sample, rate = librosa.load(path, sr=None)
    #uncomment the following lines if the files are not preproccesed yet, this does however slow
    #down the ingestion of data significantly:

    #fileLength = len(sample) / rate
    #ratioToDesired = fileLength / 30
    #sample = librosa.effects.time_stretch(sample, ratioToDesired)

    specgram = librosa.feature.mfcc(sample, n_mfcc=13, sr=rate)
    return specgram


def raw_audio(file_list, path, rate, num_idx, split):
    '''
    create the model input with the mfcc form
    Args:
        file_list (narray): list of file name get from data split
        path (str): data fold path
        rate (int): sample rate
        num_idx (int): split every mfcc matrix cross the time axis into k parts
        split (str): "train" or "val"
    '''
    sample_set = []
    index_set = []
    seq_len_set = []

    try:
        with tqdm(file_list) as t:
            for files in t:

                abs_path = os.path.join(path, files)
                mfcc = audio_read(abs_path, rate)
                sample_set.append(mfcc)

                seq_len = np.shape(mfcc)[1]
                clip_len = int(np.floor(seq_len/num_idx))
                data_idx = index_seq_build(seq_len, clip_len, num_idx, split)
                index_set.append(data_idx)
                seq_len_set.append(clip_len)

    except KeyboardInterrupt:
        t.close()
        raise
    t.close()

    return sample_set, index_set, seq_len_set


def index_seq_build(seq_len, clip_len, num_idx, split):
    '''
    create index sequence
    '''
    if split == "train":
        data_idx = np.arange(0, seq_len, clip_len)
    elif split == "val":
        #print(seq_len-clip_len)
        #print(num_idx)
        data_idx = [0]#np.random.choice(
            #seq_len-clip_len, size=num_idx, replace=False)
        #print(data_idx)
    else:
        raise NotImplementedError("split {} is not supported".format(split))

    return data_idx


def get_keyframe(datas, labels, index_list):
    assert len(datas) == len(labels)
    keyframe_indices = []

    for idx, (sample, label) in enumerate(zip(datas, labels)):
        audio_idx = index_list[idx]
        for point in audio_idx:
            keyframe_indices.append(
                (idx, point, sample, label)
            )

    return keyframe_indices
