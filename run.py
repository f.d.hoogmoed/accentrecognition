import matplotlib.pyplot as plt
import librosa.display
from torchsummary import summary
import torch
import torch.nn as nn
import argparse
import audio_model as audio_model
import datasets as datasets
import data_helper


def cmd():
    '''
    argparse function, an example to run the cod is python run.py -path -\archive\recordings\recordings
    '''
    args = argparse.ArgumentParser(description='set the config')
    args.add_argument('-path', type=str, default=r"C:\Users\Frank\PycharmProjects\AccentRecognition2\recordings\recordingsWavStretched",
                      help='type the full video path ')
    args.add_argument('-sample_rate', type=int,
                      default=22050, help='audio sample rate')
    args.add_argument('-topt', type=int, default=5,
                      help='choose the top frequency audio')
    args.add_argument('-epoch', type=int, default=30)
    args.add_argument('-num_index', type=int,
                      default=1, help='number of clip for each audio')
    args.add_argument('-model_selection', type=str, default='CNN',
                      help="Write the type of network you wanna use, CNN for convolutional, LN for linear, etc")
    args = args.parse_args()
    return args


DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")

def train(args):
    '''
    train and test the model
    '''
    train_loader = datasets.get_dataset(args, "train")
    model = audio_model.classifer(args).to(DEVICE)
    #print(summary(model, (1,13,323)))
    optimizer = torch.optim.Adam(model.parameters(), lr=0.001)

    for epoch in range(args.epoch):
        correct = 0
        epoch_loss = 0
        for i, data in enumerate(train_loader, 0):
            inputs, labels = data
            inputs, labels = inputs.to(DEVICE), labels.to(
                DEVICE, dtype=torch.long)

            optimizer.zero_grad()
            preds = model(inputs)
            #print(preds)
            #print(labels)
            loss_func = nn.CrossEntropyLoss(reduction="mean")
            loss = loss_func(preds, labels)
            loss.backward()
            optimizer.step()

            pred_result = preds.max(1, keepdim=True)[1]
            #print(pred_result)
            correct += pred_result.eq(labels.view_as(pred_result)).sum().item()
            epoch_loss += loss.item()
        correct = float(correct)/len(train_loader.dataset)
        epoch_loss /= len(train_loader.dataset)

        print("Train Epoch: {} \tloss:{:.6f} \tacc:{:.6f}".format(
            epoch+1, epoch_loss, correct))
        if (correct >= 0.999):
            break

    print("start loading validation data")
    test_loader = datasets.get_dataset(args, "val")
    test(model, test_loader)


def test(model, test_loader):
    model.eval()
    confusion_matrix = torch.zeros(args.topt, args.topt)
    test_loss = 0
    correct = 0
    with torch.no_grad():
        for inputs, labels in test_loader:
            inputs, labels = inputs.to(DEVICE), labels.to(
                DEVICE, dtype=torch.long)
            preds = model(inputs)
            loss_func = nn.CrossEntropyLoss(reduction="mean")
            loss = loss_func(preds, labels)
            test_loss += loss.item()

            pred_result = preds.max(1, keepdim=True)[1]
            for t, p in zip(labels.view(-1), pred_result.view(-1)):
                confusion_matrix[t.long(), p.long()] += 1
            #torch.where(labels == 0 & pred_result == labels, pred_result, labels)
            correct += pred_result.eq(labels.view_as(pred_result)).sum().item()
    print(confusion_matrix)
    test_loss /= len(test_loader.dataset)
    print('\ntest loss={:.4f}, accuracy={:.4f}\n'.format(
        test_loss, float(correct) / len(test_loader.dataset)))


if __name__ == "__main__":
    args = cmd()
    train(args)
    #spec = data_helper.audio_read("C:\\Users\\Frank\\PycharmProjects\\AccentRecognition2\\recordings\\recordingsWavStretched\\english126.wav", args.sample_rate)
    #fig, ax = plt.subplots()
    #img = librosa.display.specshow(spec, sr=22050, x_axis='time')
    #fig.colorbar(img, ax=ax)
    #plt.show()
