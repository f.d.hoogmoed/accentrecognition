import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np


class classifer(nn.Module):

    def __init__(self, args):
        super(classifer, self).__init__()
        self.model = args.model_selection
        if self.model == "LN":
            self.fc1 = nn.Sequential(
                nn.Linear(13, 256),
                nn.ReLU(),
            )

            self.fc2 = nn.Sequential(
                nn.Linear(256, 128),
                nn.ReLU(),
            )

            self.fc3 = nn.Sequential(
                nn.Linear(128, 64),
                nn.ReLU(),

            )

            self.fc4 = nn.Sequential(
                nn.Linear(64, args.topt),
                nn.Softmax(),
            )
        elif self.model == "CNN":
            #input = 13 * 323 * 1
            self.conv1 = nn.Conv2d(in_channels=1, out_channels=32, kernel_size=[3,3])
            #output = 13 * 323 * 32
            self.batch1 = nn.BatchNorm2d(32)
            self.relu1 = nn.ReLU()
            self.pool1 = nn.MaxPool2d(kernel_size=2, stride=2)
            #output 6 * 161 * 32
            #input 6 * 161 * 32
            self.conv2 = nn.Conv2d(in_channels=32, out_channels=64, kernel_size=[3,3])
            #output 6 * 161 * 32
            self.batch2 = nn.BatchNorm2d(64)
            self.relu2 = nn.ReLU()
            self.pool2 = nn.MaxPool2d(kernel_size=2)
            #output 3 * 80 * 64
            self.flat = nn.Flatten
            # by numindex 4 = 20736
            # by numindex 1 = 82688
            #144704
            self.fc = nn.Linear(20544, 128)
            self.dropout1 = nn.Dropout(0.25)
            self.dropout2 = nn.Dropout(0.5)
            self.dense = nn.Linear(128, 5)
    def forward(self, x):

        if self.model == "LN":
            x = self.fc1(x)
            x = self.fc2(x)
            x = self.fc3(x)
            x = F.dropout(x)
            x = self.fc4(x)
        elif self.model == "CNN":

            # Conv 1
            x = self.conv1(x)
            x = self.relu1(x)
            x = self.pool1(x)

            x = self.conv2(x)
            x = self.relu2(x)
            x = self.pool2(x)

            x = self.dropout1(x)

            x = x.view(x.size(0), -1)
            #print(x.shape)
            x = self.fc(x)

            x = self.relu1(x)
            x = self.dropout2(x)
            #print(x.shape)
            x = self.dense(x)



            # Max Pool 1


            # Conv 2

            #x = self.batch2(x)

            #x = self.dropout2(x)
            # Max Pool 2
            #print(x.shape)


            # Linear Layer
            #x = self.flat(x)
            #print(x)
            #print(x.shape)


        return x
